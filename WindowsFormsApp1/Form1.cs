﻿//
// dmProject2. Created for alternative exam
//

// Windows Libraries
using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Text.RegularExpressions;
// Visual Metro Framework
using MetroFramework;
using MetroFramework.Forms;
// Wolfram Mathematica
using Wolfram.NETLink;
// Wolfram Libraries
using WolframAlphaNET;
using WolframAlphaNET.Objects;
// Json
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Collections;

//
// Да, вычисления в облаке, даже с медленным интернет соединением, 
// гораздо быстрее, чем вычисления на локальной машине без Mathematica.
// 
// Обо всех вычислениях рассказано гораздо более подробно в презентации.
//

namespace dmProject2
{
    public partial class Form1 : MetroForm
    {
        // Простые числа до 10. Зачем? Так проще.
        int[] prime = { 2, 3, 5, 7 };

        // 
        private Polynomial.Poly TestPoly, CheckPoly;

        public Form1()
        {
            // Инициализация формы
            InitializeComponent();

            // Первичная инициализация выключателей
            if (Properties.Settings.Default.localMath)
                metroToggle1.Checked = true;
            else
                metroToggle1.Checked = false;
            if (Properties.Settings.Default.useWolfram)
                metroToggle2.Checked = true;
            else
                metroToggle2.Checked = false;

            // Задаем дефолтный файл для диалога открытия. 
            // Сохраняем то мы туда - куда хотим МЫ, а вот открыть можно любой .json файл
            openFileDialog1.FileName = Application.StartupPath + "\\" + openFileDialog1.FileName;
            saveFileDialog1.FileName = Application.StartupPath + "\\" + saveFileDialog1.FileName;

            int a = Convert.ToInt32(Math.IEEERemainder(-1, 3));
        }

        //
        // Запуск генерации многочленов
        //
        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(maxDegreeBox.Text) || Convert.ToInt32(maxDegreeBox.Text) < 1 || Convert.ToInt32(maxDegreeBox.Text) > 9)
                return;

            if (!Properties.Settings.Default.localMath && Properties.Settings.Default.useWolfram)
            {
                string message =
                     "Генерация полиномов не работает без предустановленного пакета Wolfram Mathematica. " +
                      "Почему? Потому что количество вычислений высоко. Работа программы затянется на неопределенный срок. ";
                //"Wolfram Language - функциональный язык программирования. Он позволяет в разы сократить время генерации." +
                //"Но даже с ним - генерация занимает приличный срок.";
                MetroMessageBox.Show(this, message, "Информация", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if(Convert.ToInt32(maxDegreeBox.Text) > 5)
            {
                // Предупреждаем пользователя что нажатие кнопки - опасно
                string msg = "Вы уверены? Это долгий процесс для степеней большей 5. В комплекте должен был идти уже заранее сгенерированный файл";
                DialogResult dialogResult = MetroMessageBox.Show(this, msg, "Информация", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dialogResult == DialogResult.No)
                {
                    return;
                }
            }

            // Отключаем кнопку
            metroButton1.Enabled = false;

            // Отделльно храним максимальную степен
            int mDeg = Convert.ToInt32(maxDegreeBox.Text);

            // Lets try something new 
            // База для файла
            JArray json = new JArray();

            JArray[] pols = { new JArray(), new JArray(), new JArray(), new JArray() };

            for (int l = 0; l < pols.Length; l++)
            {
                json.Add(
                    new JObject(
                        new JProperty("modulus", prime[l]),
                        new JProperty("polynomials", pols[l])
                    )
                );
            }

            // Ну и основной цикл по генерации многочленов
            // p - Zp; n - максимальная степень
            int[] coef = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            for (int i = 1, p = prime.Last(), n = mDeg; i < Math.Pow(p, n + 1); i++)
            {
                if (n < coef.Length)
                    coef[n] = i % p;
                // Когда i пропорциональна р, младший коэффициент полинома 0, поэтому
                // мы увеличиваем значение предшествующего ему коэффициента на "1".
                // Если от добавления этой единицы обнулился коэффициент,
                // то мы добавляем эту "1" ещё и к предшествующему(внутренний цикл).
                if (i % p == 0)
                {
                    for (int j = n - 1; j >= 0; j--)
                    {
                        coef[j] += 1;
                        if (coef[j] % p == 0)
                        {
                            coef[j] = 0;
                        }
                        else
                            break;
                    }
                }
                //Проверка многочлена и, если он неразложим, запись в БД
                GenerateAndCheckAndWriteInAllZ(coef, json);
            }

            // Записываем всё в файл
            File.WriteAllText(saveFileDialog1.FileName, json.ToString());

            // Включаем обратно кнопку
            metroButton1.Enabled = true;
        }

        private void GenerateAndCheckAndWriteInAllZ(int[] coeff, JArray json)
        {
            // Проверяем, не является ли многочлен констаной
            int pos = 0;
            for (int i = coeff.Length - 2; pos < 1 && i >= 1; i--)
            {
                if (coeff[i] > 0)
                    pos++;
            }
            if (pos < 1)
                return;

            // Создаем полином по массиву коэффициентов
            string tmpStr = Regex.Replace(GeneratePolynomial(coeff).ToString(), @"\s+", "");
            Polynomial.Poly polynomial = new Polynomial.Poly(tmpStr);

            // Проверяем разложимость в каждом Zp
            for (int i = 0; i < prime.Length; i++)
            {
                // Если значение коэфф. > текущего простого числа -> выкидываем полином
                //if (Convert.ToBoolean(coeff.FirstOrDefault(o => o >= prime[i])))
                if (Convert.ToBoolean(coeff.FirstOrDefault(o => o >= prime[i])))
                    continue;

                if (Properties.Settings.Default.useWolfram)
                {
                    // Проверка на неразложимость
                    if (InDeCom(polynomial.ToString(), prime[i]))
                    {
                        Console.WriteLine(polynomial + " Неприводим в Z" + prime[i]);

                        JArray pls = (JArray)json[i].SelectToken("polynomials");
                        pls.Add(Regex.Replace(polynomial.ToString(), @"\s+", ""));
                    }
                    else
                    {
                        Console.WriteLine(polynomial + " Приводим в Z" + prime[i]);
                    }
                }
                else
                {
                    // Проверка на неразложимость по базе
                    JArray pls = (JArray)json[i].SelectToken("polynomials");

                    if(polynomial.Terms.MaxTermsPower() == 1)
                    {
                        pls.Add(Regex.Replace(polynomial.ToString(), @"\s+", ""));
                        break;
                    }

                    bool irreducible = false;
                    foreach (JValue a in pls)
                    {
                        string tmpStr2 = a.Value.ToString();
                        Polynomial.Poly tmpPol = new Polynomial.Poly(tmpStr2);
                        // Если многочлен первой степени - добавляем в список неприводимых
                        if (tmpPol.Terms.MaxTermsPower() >= polynomial.Terms.MaxTermsPower())
                            continue;

                        // Если делится без остатка - приводим
                        //Polynomial.Poly tmpPol2 = polynomial % tmpPol;
                        Polynomial.Poly tmpPol2 = Polynomial.Poly.PolyMod2(polynomial, tmpPol, prime[i]);
                        if (tmpPol2.ToString() == "0" || tmpPol2.ToString() == "")
                        {
                            Console.WriteLine(polynomial + " Приводим в Z" + prime[i]);
                            irreducible = false;
                            break;
                        }
                        // Иначе - неприводим
                        else
                        {
                            Console.WriteLine(polynomial + " Неприводим в Z" + prime[i]);
                            irreducible = true;
                        }                            
                    }

                    // если многочлен неприводим - добаваляем в список
                    if(irreducible)
                        pls.Add(Regex.Replace(polynomial.ToString(), @"\s+", ""));
                }
            }
        }

        // Not used. Dev-only
        private void metroButton2_Click(object sender, EventArgs e)
        {
            if (Polynomial.Poly.ValidateExpression(metroTextBox1.Text))
            {
                TestPoly = new Polynomial.Poly(metroTextBox1.Text);
                CheckPoly = new Polynomial.Poly(metroTextBox3.Text);
                TestPoly = TestPoly % CheckPoly;
            }
            else
            {
                return;
            }
        }

        //
        // Проверка многочлена на неразложимость по заранее созданному файлу
        //
        private void metroButton3_Click(object sender, EventArgs e)
        {
            try
            {
                // Вырубаем кнопочку
                metroButton3.Enabled = false;
                // Считываем полином и проверяем его на правильность
                Polynomial.Poly poly;
                if (Polynomial.Poly.ValidateExpression(polynomialBox.Text))
                {
                     poly = new Polynomial.Poly(polynomialBox.Text);
                }
                else
                {
                    return;
                }                
                // Считываем JSON из файла
                using (StreamReader file = File.OpenText(openFileDialog1.FileName))
                using (JsonTextReader reader = new JsonTextReader(file))
                {
                    JArray o1 = (JArray)JToken.ReadFrom(reader);

                    foreach (JObject o2 in o1)
                    {
                        int mod = (Int32)o2.SelectToken("modulus");

                        Polynomial.Poly newPoly = poly % mod;
                        if (newPoly.Terms.MaxTermsPower() == 0)
                            continue;

                        // Проверяем, есть ли в нашей базе такой многочлен. 
                        // Если есть => исходный неприводим
                        foreach (JToken o3 in (JArray)o2.SelectToken("polynomials"))
                        {

                            if (o3.ToString() == Regex.Replace(newPoly.ToString(), @"\s+", ""))
                            {
                                MetroMessageBox.Show(
                                    this,
                                    "Многочлен " + polynomialBox.Text + " неприводим над полем Q, т.к. неприводим в поле вычетов Z/" + mod + "Z",
                                    "Проверка приводимости",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information
                                );
                                metroButton3.Enabled = true;
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception ef)
            {
                MetroMessageBox.Show(this, ef.Message.ToString() + "\n\nRestart the program!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                metroButton3.Enabled = true;
            }

            MetroMessageBox.Show(
                this,
                "Многочлена " + polynomialBox.Text + " нет в списке неприводимых.",
                "Проверка приводимости",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information
            );
        }

        //
        // Диалог открытия файла
        //
        private void metroButton4_Click(object sender, EventArgs e)
        {
            Console.WriteLine(openFileDialog1.FileName);
            openFileDialog1.Filter = "JSON(*.json) | *.json";
            openFileDialog1.InitialDirectory = Application.StartupPath;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Console.WriteLine(openFileDialog1.FileName);
                metroTextBox2.Text = openFileDialog1.FileName;
            }
        }

        //
        // Разложение многочлена
        //
        private void metroButton5_Click(object sender, EventArgs e)
        {
            metroButton5.Enabled = false;

            string newQuery = "Factor[" + polyQBox.Text + ", Modulus->" + modQBox.Text + "]";

            Image q = null;
            try
            {
                q = WolframQuery(newQuery);
            }
            catch (Exception ef)
            {
                MetroMessageBox.Show(this, ef.Message.ToString() + "\n\nRestart the program!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                pictureBox2.Image = q;
            }

            metroButton5.Enabled = true;
        }
        private void metroButton6_Click(object sender, EventArgs e)
        {
            Console.WriteLine(saveFileDialog1.FileName);
            saveFileDialog1.Filter = "JSON(*.json) | *.json";
            saveFileDialog1.InitialDirectory = Application.StartupPath;

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                Console.WriteLine(saveFileDialog1.FileName);
                metroTextBox4.Text = saveFileDialog1.FileName;
            }

        }

        private void metroToggle1_CheckedChanged(object sender, EventArgs e)
        {
            if (metroToggle1.Checked)
            {
                Properties.Settings.Default.localMath = true;
            }
            else
            {
                Properties.Settings.Default.localMath = false;
            }
            Properties.Settings.Default.Save();
            Console.WriteLine(Properties.Settings.Default.localMath);
        }

        private void metroToggle2_CheckedChanged(object sender, EventArgs e)
        {
            if (metroToggle2.Checked)
            {
                Properties.Settings.Default.useWolfram = true;
            }
            else
            {
                Properties.Settings.Default.useWolfram = false;
            }
            Properties.Settings.Default.Save();
            Console.WriteLine(Properties.Settings.Default.useWolfram);
        }

        //
        // Генерируем строку полинома из массива коэффициентов
        //
        private string GeneratePolynomial(int[] coeff)
        {
            string polynomial = null;
            int i = coeff.Length - 1;
            foreach (int c in coeff.Reverse())
            {
                if (c > 0)
                {
                    if (i == 0 || c > 1)
                        polynomial += c;

                    if (i > 1)
                        polynomial += "x^" + i;
                    else if (i == 1)
                        polynomial += "x";

                    polynomial += " + ";
                }
                i--;
            }
            polynomial = polynomial.Trim();
            if (polynomial.Last() == '+')
            {
                polynomial = polynomial.Remove(polynomial.Length - 1);
            }

            return polynomial;
        }

        private Image WolframQuery(string query)
        {
            if (Properties.Settings.Default.localMath)
            {

                mathKernel1.ResultFormat = MathKernel.ResultFormatType.TraditionalForm;
                try
                {
                    mathKernel1.Input = query;
                    mathKernel1.Compute();
                }
                catch (Exception ef)
                {
                    MetroMessageBox.Show(this, ef.Message.ToString() + "\n\nRestart the program!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                return (Image)mathKernel1.Result;
            }
            else
            {
                //First create the main class:
                WolframAlpha wolfram = new WolframAlpha(Properties.Settings.Default._apiKey);

                //Then you simply query Wolfram|Alpha like this
                //Note that the spelling error will be correct by Wolfram|Alpha
                wolfram.IncludePodIDs.Add("Result");
                wolfram.Formats.Add(WolframAlphaNET.Enums.Format.Image);
                QueryResult results = wolfram.Query(query);

                //The QueryResult object contains the parsed XML from Wolfram|Alpha. Lets look at it.
                //The results from wolfram is split into "pods". We just print them.
                if (results != null)
                {
                    if (results.Pods.Count > 0)
                    {
                        if (results.Pods[0].SubPods[0].Image.Alt == "(irreducible)")
                            return null;

                        WebClient wc = new WebClient();
                        byte[] bytes = wc.DownloadData(results.Pods[0].SubPods[0].Image.Src);
                        MemoryStream ms = new MemoryStream(bytes);
                        Image img = Image.FromStream(ms);

                        return img;
                    }
                }
            }

            return null;
        }

        private string WolframLocQueryStr(string query)
        {
            mathKernel1.ResultFormat = MathKernel.ResultFormatType.InputForm;
            try
            {
                mathKernel1.Input = query;
                mathKernel1.Compute();
            }
            catch (Exception ef)
            {
                MetroMessageBox.Show(this, ef.Message.ToString() + "\n\nRestart the program!", "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            }

            return mathKernel1.Result.ToString();
        }

        private string WolframAlphaQueryStr(string query)
        {
            if (!Properties.Settings.Default.localMath)
            {
                //First create the main class:
                WolframAlpha wolfram = new WolframAlpha(Properties.Settings.Default._apiKey);

                //Then you simply query Wolfram|Alpha like this
                //Note that the spelling error will be correct by Wolfram|Alpha
                wolfram.IncludePodIDs.Add("Result");
                wolfram.Formats.Add(WolframAlphaNET.Enums.Format.Plaintext);
                QueryResult results = wolfram.Query(query);

                //The QueryResult object contains the parsed XML from Wolfram|Alpha. Lets look at it.
                //The results from wolfram is split into "pods". We just print them.
                if (results != null)
                {
                    if (results.Pods.Count > 0)
                    {
                        return Regex.Replace(results.Pods[0].SubPods[0].Plaintext, @"\s+", "");
                    }
                }
            }

            return null;
        }

        //
        // Проверка многочлена на разложимость в Zp
        //
        private bool InDeCom(string pol, int modulus = 0, bool mod = false)
        {
            // Убираем все пробелы в выражении
            pol = Regex.Replace(pol, @"\s+", "");

            // Локальные вычисления
            // Должна быть предустановлена Mathematica
            if (Properties.Settings.Default.localMath)
            {
                if (Convert.ToBoolean(WolframLocQueryStr("IrreduciblePolynomialQ[" + pol + ", Modulus->" + modulus + "]")))
                {
                    return true;
                }
            }
            // Вычисления в облаке. Должно работать
            else
            {
                string newPol = pol;

                // Создаем экземпляр объекта
                WolframAlpha wolfram = new WolframAlpha(Properties.Settings.Default._apiKey);
                QueryResult results = null;

                // Запро  в облако
                wolfram.IncludePodIDs.Add("Result");
                wolfram.Formats.Add(WolframAlphaNET.Enums.Format.Plaintext);

                // Вот тут мы приводим многочлен к нужному нам виду
                if (mod)
                {
                    string query1 = "PolynomialMod[" + pol + ", " + modulus + "]";
                    results = wolfram.Query(query1);
                    // Проверяем первый запрос
                    if (results != null)
                    {
                        if (results.Pods.Count > 0)
                        {
                            // Записываем значение во временную переменную
                            newPol = Regex.Replace(results.Pods[0].SubPods[0].Plaintext, @"\s+", "");
                        }
                    }
                }

                // Формируем запрос
                // string query2 = "Expand[Factor[" + newPol + ", Modulus->" + modulus + "]]";
                string query2 = "IrreduciblePolynomialQ[" + pol + "]";
                // Отправляем и получаем результаты
                results = wolfram.Query(query2);
                // Проверяем второй запрос
                if (results != null)
                {
                    if (results.Pods.Count > 0)
                    {
                        if (Convert.ToBoolean(results.Pods[0].SubPods[0].Plaintext))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

    }
}
