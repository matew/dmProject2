﻿using System;

namespace Polynomial
{
    public class Poly
    {
        #region Конструкторы:

        public Poly(string PolyExpression)
        {
            this._Terms = new TermCollection();
            this.ReadPolyExpression(PolyExpression);
        }

        public Poly(TermCollection terms)
        {
            this.Terms = terms;
            this.Terms.Sort(TermCollection.SortType.ASC);
        }

        #endregion

        #region Деструктор:
        /// <summary>
        /// Clear the Term Collections
        /// </summary>
        ~Poly()
        {
            this.Terms.Clear();
        }

        #endregion 

        #region Переопределяемые методы:

        public override string ToString()
        {
            //if(this.Lentgh == 0)

            this.Terms.Sort(TermCollection.SortType.DES);

            string result = string.Empty;
            foreach (Term t in this.Terms)
            {
                result += t.ToString();
            }
            if (result.ToString() != "" && result.Substring(0, 1) == "+")
                result = result.Remove(0, 1);
            return result;
        }

        #endregion

        #region Методы:

        public long Calculate(int x)
        {
            long result = 0;
            foreach (Term t in this.Terms)
            {
                result += (long)t.Coefficient * (long)(Math.Pow(x, t.Power));
            }
            return result;
        }

        public static bool ValidateExpression(string Expression)
        {
            if (Expression.Length == 0)
                return false;

            Expression = Expression.Trim();
            Expression = Expression.Replace(" ", "");
            while (Expression.IndexOf("--") > -1 | Expression.IndexOf("++") > -1 | Expression.IndexOf("^^") > -1 | Expression.IndexOf("xx") > -1)
            {
                Expression = Expression.Replace("--", "-");
                Expression = Expression.Replace("++", "+");
                Expression = Expression.Replace("^^", "^");
                Expression = Expression.Replace("xx", "x");
            }
            string ValidChars = "+-x1234567890^";
            bool result = true;
            foreach (char c in Expression)
            {
                if (ValidChars.IndexOf(c) == -1)
                    result = false;
            }
            return result;
        }

        private void ReadPolyExpression(string PolyExpression)
        {
            if(ValidateExpression(PolyExpression))
            {
                string NextChar = string.Empty;
                string NextTerm = string.Empty;
                for (int i = 0 ; i < PolyExpression.Length; i++)
                {
                    NextChar = PolyExpression.Substring(i, 1);
                    if ((NextChar == "-" | NextChar == "+") & i > 0)
                    {
                        Term TermItem = new Term(NextTerm);
                        this.Terms.Add(TermItem);
                        NextTerm = string.Empty;
                    }
                    NextTerm += NextChar;
                }
                Term Item = new Term(NextTerm);
                this.Terms.Add(Item);
                
                this.Terms.Sort(TermCollection.SortType.ASC);
            }
            else
            {
                //throw new Exception("Invalid Polynomial Expression");
                return;
            }
        }

        #endregion

        #region Поля и Свойства:
 
        private TermCollection _Terms;
        public TermCollection Terms
        {
            get
            {
                return _Terms;
            }
            set
            {
                _Terms = value;
            }
        }


        // 
        // Длина строки
        //
        public int Lentgh
        {
            get
            {
                return this.Terms.Length;
            }
        }
        #endregion

        #region Свои операторы:

        // 
        // Сложение двух многочленов
        //
        public static Poly operator +(Poly p1, Poly p2)
        {
            Poly result = new Poly(p1.ToString());
            foreach (Term t in p2.Terms)
                result.Terms.Add(t);
            return result;
        }

        // 
        // Разность двух многочленов
        //
        public static Poly operator -(Poly p1, Poly p2)
        {
            Poly result = new Poly(p1.ToString());
            Poly NegetiveP2 = new Poly(p2.ToString());
            foreach (Term t in NegetiveP2.Terms)
                t.Coefficient *= -1;

            return result + NegetiveP2;
        }
       
        //
        // Умножение двух многочленов
        //
        public static Poly operator *(Poly p1, Poly p2)
        {
            TermCollection result = new TermCollection();
            int counter = 0;
            foreach (Term t1 in p1.Terms)
            {
                foreach (Term t2 in p2.Terms)
                {
                    result.Add(new Term(t1.Power + t2.Power,t1.Coefficient * t2.Coefficient));
                    counter++;
                }
            }
            return new Poly(result);
        }

        // 
        // Частное от деления двух многочленов
        //
        public static Poly operator /(Poly p1, Poly p2)
        {
            p1.Terms.Sort(TermCollection.SortType.DES);
            p2.Terms.Sort(TermCollection.SortType.DES);
            TermCollection resultTerms = new TermCollection();
            if (p1.Terms[0].Power < p2.Terms[0].Power)
                throw new Exception("Ошибка при делении: P1.MaxPower меньше P2.MaxPower");
            while(p1.Terms[0].Power > p2.Terms[0].Power)
            {
                Term NextResult = new Term(p1.Terms[0].Power - p2.Terms[0].Power, p1.Terms[0].Coefficient / p2.Terms[0].Coefficient);
                resultTerms.Add(NextResult);
                Poly TempPoly = NextResult;

                Poly NewPoly = TempPoly * p2;
                p1 = p1 - NewPoly;
            }
            return new Poly(resultTerms);
        }

        // 
        // Остаток от деления двух многочленов
        //
        public static Poly operator %(Poly p1, Poly p2)
        {
            p1.Terms.Sort(TermCollection.SortType.DES);
            p2.Terms.Sort(TermCollection.SortType.DES);
            TermCollection resultTerms = new TermCollection();
            if (p1.Terms[0].Power < p2.Terms[0].Power)
                throw new Exception("Ошибка при делении: P1.MaxPower меньше P2.MaxPower");
            while (p1.Terms[0].Power > p2.Terms[0].Power)
            {
                Term NextResult = new Term(p1.Terms[0].Power - p2.Terms[0].Power, p1.Terms[0].Coefficient / p2.Terms[0].Coefficient);
                resultTerms.Add(NextResult);
                Poly TempPoly = NextResult;

                Poly NewPoly = TempPoly * p2;
                p1 = p1 - NewPoly;
            }

            return new Poly(p1.ToString());
        }    
        
        // 
        // Остаток от деления двух многочленов
        //
        public static Poly PolyMod2(Poly A, Poly B, int mod)
        {
            A.Terms.Sort(TermCollection.SortType.DES);
            B.Terms.Sort(TermCollection.SortType.DES);
            TermCollection resultTerms = new TermCollection();
            if (A.Terms[0].Power < B.Terms[0].Power)
                throw new Exception("Ошибка при делении: P1.MaxPower меньше P2.MaxPower");

            Poly R = A;
            Poly Q = "0";

            while(R != null && R.Terms.MaxTermsPower() >= B.Terms.MaxTermsPower())
            {
                Poly T = ExtDiv(R.Terms[0].Coefficient, B.Terms[0].Coefficient, mod) + "x^" + (R.Terms.MaxTermsPower() - B.Terms.MaxTermsPower());
                Q = Q % mod;
                T = T % mod;
                Q = Q + T;
                Q = Q % mod;
                R = R - ((T * B) % mod);
                R = R % mod;
            }

            return new Poly(R.ToString());
        }

        // 
        // Переход на модульную арифметику. От каждого коэффициента оставляем остаток от деления на число
        //
        public static Poly operator %(Poly p1, int mod)
        {
            Poly result = new Poly(p1.ToString());
            foreach (Term t in result.Terms)
                t.Coefficient = (t.Coefficient % mod + mod) % mod;

            return result;
        }

        public static int ExtDiv(int a, int b, int mod)
        {
            int i = 1, tmp = a;
            while (tmp != b)
            {
                tmp = (tmp + a) % mod;
                i++;
            }

            return i;
        }

        // 
        // Переход на модульную арифметику
        //
        public static Poly DivPoly(Poly p1, int div, int mod)
        {
            Poly result = new Poly(p1.ToString());
            foreach (Term t in result.Terms)
            {
                int i = 1, tmp = t.Coefficient;
                while (t.Coefficient < div)
                {
                    tmp = (tmp + t.Coefficient) % div;
                    i++;
                }
            }

            return result;
        }

        public static Poly operator ++(Poly p1)
        {
            Poly p2 = new Poly("1");
            p1 = p1 + p2;
            return p1;
        }

        public static Poly operator --(Poly p1)
        {
            Poly p2 = new Poly("-1");
            p1 = p1 + p2;
            return p1;
        }

        //
        // Создание многочлена
        //
        public static implicit operator Poly(Term t)
        {
            TermCollection Terms = new TermCollection();
            Terms.Add(t);
            return new Poly(Terms);
        }

        public static implicit operator Poly(string expression)
        {
            return new Poly(expression);
        }

        public static implicit operator Poly(int value)
        {
            return new Poly(value.ToString());
        }
        #endregion
    }
}
