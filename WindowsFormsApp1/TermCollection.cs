﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace Polynomial
{
    public class TermCollection:CollectionBase
    {

        #region Перечисляемые типы:

        // Сортировка
        public enum SortType
        {
            ASC = 0,
            DES = 1
        }

        #endregion

        #region Свои методы:

        public void Sort(SortType Order)
        {
            TermCollection result = new TermCollection();
            if (Order == SortType.ASC)
            {
                while (this.Length > 0)
                {
                    Term MinTerm = this[0];
                    foreach (Term t in List)
                    {
                        if (t.Power < MinTerm.Power)
                        {
                            MinTerm = t;
                        }
                    }
                    result.Add(MinTerm);
                    this.Remove(MinTerm);
                }
            }
            else
            {
                while (this.Length > 0)
                {
                    Term MaxTerm = this[0];
                    foreach (Term t in List)
                    {
                        if (t.Power > MaxTerm.Power)
                        {
                            MaxTerm = t;
                        }
                    }
                    result.Add(MaxTerm);
                    this.Remove(MaxTerm);
                }
            }
            
            this.Clear();
            foreach (Term t in result)
            {
                this.Add(t);
            }
        }

        public void AddToEqualPower(Term value)
        {
            foreach (Term t in List)
            {
                if (t.Power == value.Power)
                    t.Coefficient += value.Coefficient;
            }
        }

        public bool HasTermByPower(int p)
        {
            foreach (Term t in List)
            {
                if (t.Power == p)
                    return true;
            }
            return false;
        }

        public int MaxTermsPower()
        {
            int maxPower = 0;
            foreach (Term t in List)
            {
                if (t.Power > maxPower)
                    maxPower = t.Power;
            }

            return maxPower;
        }
        #endregion

        #region Члены коллекции:

        public Term this[int index]
        {
            get { return ((Term)List[index]); }
            set { List[index] = value; }
        }
        
        public int Length
        {
            get
            {
                return List.Count;
            }
        }

        // Добавляем
        public int Add(Term value)
        {
            if (value.Coefficient != 0)
            {
                if (this.HasTermByPower(value.Power))
                {
                    this.AddToEqualPower(value);
                    return -1;
                }
                else
                    return (List.Add(value));
            }
            else
                return -1;
        }
        

        public int IndexOf(Term value)
        {
            return (List.IndexOf(value));
        }

        public void Insert(int index, Term value)
        {
            List.Insert(index, value);
        }

        public void Remove(Term value)
        {
            List.Remove(value);
        }

        public bool Contatins(Term value)
        {
            return (List.Contains(value));
        }

        #endregion

    }
}
