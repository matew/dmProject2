﻿namespace dmProject2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.mathKernel1 = new Wolfram.NETLink.MathKernel();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.metroButton4 = new MetroFramework.Controls.MetroButton();
            this.metroTextBox2 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.polynomialBox = new MetroFramework.Controls.MetroTextBox();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroButton6 = new MetroFramework.Controls.MetroButton();
            this.metroTextBox4 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel11 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel10 = new MetroFramework.Controls.MetroLabel();
            this.metroTextBox3 = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.maxDegreeBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage3 = new MetroFramework.Controls.MetroTabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.modQBox = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.polyQBox = new MetroFramework.Controls.MetroTextBox();
            this.metroButton5 = new MetroFramework.Controls.MetroButton();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.metroToggle1 = new MetroFramework.Controls.MetroToggle();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.metroToolTip1 = new MetroFramework.Components.MetroToolTip();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroToggle2 = new MetroFramework.Controls.MetroToggle();
            this.metroTabControl1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.metroTabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // mathKernel1
            // 
            this.mathKernel1.AutoCloseLink = true;
            this.mathKernel1.CaptureGraphics = false;
            this.mathKernel1.CaptureMessages = true;
            this.mathKernel1.CapturePrint = true;
            this.mathKernel1.GraphicsFormat = "Automatic";
            this.mathKernel1.GraphicsHeight = 0;
            this.mathKernel1.GraphicsResolution = 0;
            this.mathKernel1.GraphicsWidth = 0;
            this.mathKernel1.HandleEvents = true;
            this.mathKernel1.Input = null;
            this.mathKernel1.Link = null;
            this.mathKernel1.LinkArguments = null;
            this.mathKernel1.PageWidth = 0;
            this.mathKernel1.ResultFormat = Wolfram.NETLink.MathKernel.ResultFormatType.InputForm;
            this.mathKernel1.UseFrontEnd = false;
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.metroTabPage1);
            this.metroTabControl1.Controls.Add(this.metroTabPage2);
            this.metroTabControl1.Controls.Add(this.metroTabPage3);
            this.metroTabControl1.Location = new System.Drawing.Point(5, 58);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 1;
            this.metroTabControl1.Size = new System.Drawing.Size(422, 285);
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroTabControl1.TabIndex = 0;
            this.metroTabControl1.UseSelectable = true;
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.metroButton4);
            this.metroTabPage2.Controls.Add(this.metroTextBox2);
            this.metroTabPage2.Controls.Add(this.metroLabel6);
            this.metroTabPage2.Controls.Add(this.polynomialBox);
            this.metroTabPage2.Controls.Add(this.metroButton3);
            this.metroTabPage2.Controls.Add(this.metroLabel4);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.HorizontalScrollbarSize = 10;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(414, 243);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Проверка приводимости";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            this.metroTabPage2.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage2.VerticalScrollbarSize = 10;
            // 
            // metroButton4
            // 
            this.metroButton4.Location = new System.Drawing.Point(4, 35);
            this.metroButton4.Name = "metroButton4";
            this.metroButton4.Size = new System.Drawing.Size(74, 23);
            this.metroButton4.TabIndex = 10;
            this.metroButton4.Text = "Изменить";
            this.metroButton4.UseSelectable = true;
            this.metroButton4.Click += new System.EventHandler(this.metroButton4_Click);
            // 
            // metroTextBox2
            // 
            // 
            // 
            // 
            this.metroTextBox2.CustomButton.Image = null;
            this.metroTextBox2.CustomButton.Location = new System.Drawing.Point(251, 1);
            this.metroTextBox2.CustomButton.Name = "";
            this.metroTextBox2.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox2.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox2.CustomButton.TabIndex = 1;
            this.metroTextBox2.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox2.CustomButton.UseSelectable = true;
            this.metroTextBox2.CustomButton.Visible = false;
            this.metroTextBox2.Lines = new string[] {
        "polynomials.json"};
            this.metroTextBox2.Location = new System.Drawing.Point(138, 8);
            this.metroTextBox2.MaxLength = 32767;
            this.metroTextBox2.Name = "metroTextBox2";
            this.metroTextBox2.PasswordChar = '\0';
            this.metroTextBox2.ReadOnly = true;
            this.metroTextBox2.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox2.SelectedText = "";
            this.metroTextBox2.SelectionLength = 0;
            this.metroTextBox2.SelectionStart = 0;
            this.metroTextBox2.ShortcutsEnabled = true;
            this.metroTextBox2.Size = new System.Drawing.Size(273, 23);
            this.metroTextBox2.TabIndex = 9;
            this.metroTextBox2.Text = "polynomials.json";
            this.metroTextBox2.UseSelectable = true;
            this.metroTextBox2.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox2.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.Location = new System.Drawing.Point(3, 12);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(133, 19);
            this.metroLabel6.TabIndex = 8;
            this.metroLabel6.Text = "Файл для проверки:";
            // 
            // polynomialBox
            // 
            // 
            // 
            // 
            this.polynomialBox.CustomButton.AccessibleName = "asd";
            this.polynomialBox.CustomButton.Image = null;
            this.polynomialBox.CustomButton.Location = new System.Drawing.Point(386, 1);
            this.polynomialBox.CustomButton.Name = "";
            this.polynomialBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.polynomialBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.polynomialBox.CustomButton.TabIndex = 1;
            this.polynomialBox.CustomButton.Text = "Проверить";
            this.polynomialBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.polynomialBox.CustomButton.UseSelectable = true;
            this.polynomialBox.CustomButton.Visible = false;
            this.polynomialBox.IconRight = true;
            this.polynomialBox.Lines = new string[] {
        "x^8+x^7+x^6+x^5+x^4+x^3+x^2+x+2"};
            this.polynomialBox.Location = new System.Drawing.Point(3, 93);
            this.polynomialBox.MaxLength = 32767;
            this.polynomialBox.Name = "polynomialBox";
            this.polynomialBox.PasswordChar = '\0';
            this.polynomialBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.polynomialBox.SelectedText = "";
            this.polynomialBox.SelectionLength = 0;
            this.polynomialBox.SelectionStart = 0;
            this.polynomialBox.ShortcutsEnabled = true;
            this.polynomialBox.ShowClearButton = true;
            this.polynomialBox.Size = new System.Drawing.Size(408, 23);
            this.polynomialBox.Style = MetroFramework.MetroColorStyle.Orange;
            this.polynomialBox.TabIndex = 5;
            this.polynomialBox.Text = "x^8+x^7+x^6+x^5+x^4+x^3+x^2+x+2";
            this.polynomialBox.UseSelectable = true;
            this.polynomialBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.polynomialBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(4, 122);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(74, 23);
            this.metroButton3.TabIndex = 4;
            this.metroButton3.Text = "Проверить";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(3, 71);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(132, 19);
            this.metroLabel4.TabIndex = 2;
            this.metroLabel4.Text = "Введите многочлен:";
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroButton6);
            this.metroTabPage1.Controls.Add(this.metroTextBox4);
            this.metroTabPage1.Controls.Add(this.metroLabel11);
            this.metroTabPage1.Controls.Add(this.metroLabel10);
            this.metroTabPage1.Controls.Add(this.metroTextBox3);
            this.metroTabPage1.Controls.Add(this.metroTextBox1);
            this.metroTabPage1.Controls.Add(this.metroLabel9);
            this.metroTabPage1.Controls.Add(this.metroButton2);
            this.metroTabPage1.Controls.Add(this.metroButton1);
            this.metroTabPage1.Controls.Add(this.maxDegreeBox);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.HorizontalScrollbarSize = 10;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(414, 243);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Генерация многочленов";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            this.metroTabPage1.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage1.VerticalScrollbarSize = 10;
            // 
            // metroButton6
            // 
            this.metroButton6.Location = new System.Drawing.Point(4, 91);
            this.metroButton6.Name = "metroButton6";
            this.metroButton6.Size = new System.Drawing.Size(74, 23);
            this.metroButton6.TabIndex = 17;
            this.metroButton6.Text = "Изменить";
            this.metroButton6.UseSelectable = true;
            this.metroButton6.Click += new System.EventHandler(this.metroButton6_Click);
            // 
            // metroTextBox4
            // 
            // 
            // 
            // 
            this.metroTextBox4.CustomButton.Image = null;
            this.metroTextBox4.CustomButton.Location = new System.Drawing.Point(224, 1);
            this.metroTextBox4.CustomButton.Name = "";
            this.metroTextBox4.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox4.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox4.CustomButton.TabIndex = 1;
            this.metroTextBox4.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox4.CustomButton.UseSelectable = true;
            this.metroTextBox4.CustomButton.Visible = false;
            this.metroTextBox4.Lines = new string[] {
        "polynomials.json"};
            this.metroTextBox4.Location = new System.Drawing.Point(152, 68);
            this.metroTextBox4.MaxLength = 32767;
            this.metroTextBox4.Name = "metroTextBox4";
            this.metroTextBox4.PasswordChar = '\0';
            this.metroTextBox4.ReadOnly = true;
            this.metroTextBox4.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox4.SelectedText = "";
            this.metroTextBox4.SelectionLength = 0;
            this.metroTextBox4.SelectionStart = 0;
            this.metroTextBox4.ShortcutsEnabled = true;
            this.metroTextBox4.Size = new System.Drawing.Size(246, 23);
            this.metroTextBox4.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroTextBox4.TabIndex = 16;
            this.metroTextBox4.Text = "polynomials.json";
            this.metroTextBox4.UseSelectable = true;
            this.metroTextBox4.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox4.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel11
            // 
            this.metroLabel11.AutoSize = true;
            this.metroLabel11.Location = new System.Drawing.Point(3, 68);
            this.metroLabel11.Name = "metroLabel11";
            this.metroLabel11.Size = new System.Drawing.Size(146, 19);
            this.metroLabel11.TabIndex = 15;
            this.metroLabel11.Text = "Файл для сохранения:";
            // 
            // metroLabel10
            // 
            this.metroLabel10.AutoSize = true;
            this.metroLabel10.Location = new System.Drawing.Point(182, 188);
            this.metroLabel10.Name = "metroLabel10";
            this.metroLabel10.Size = new System.Drawing.Size(53, 19);
            this.metroLabel10.TabIndex = 14;
            this.metroLabel10.Text = "Кольцо";
            this.metroLabel10.Visible = false;
            // 
            // metroTextBox3
            // 
            // 
            // 
            // 
            this.metroTextBox3.CustomButton.Image = null;
            this.metroTextBox3.CustomButton.Location = new System.Drawing.Point(131, 1);
            this.metroTextBox3.CustomButton.Name = "";
            this.metroTextBox3.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox3.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox3.CustomButton.TabIndex = 1;
            this.metroTextBox3.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox3.CustomButton.UseSelectable = true;
            this.metroTextBox3.CustomButton.Visible = false;
            this.metroTextBox3.Lines = new string[] {
        "x+1"};
            this.metroTextBox3.Location = new System.Drawing.Point(162, 210);
            this.metroTextBox3.MaxLength = 32767;
            this.metroTextBox3.Name = "metroTextBox3";
            this.metroTextBox3.PasswordChar = '\0';
            this.metroTextBox3.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox3.SelectedText = "";
            this.metroTextBox3.SelectionLength = 0;
            this.metroTextBox3.SelectionStart = 0;
            this.metroTextBox3.ShortcutsEnabled = true;
            this.metroTextBox3.Size = new System.Drawing.Size(153, 23);
            this.metroTextBox3.TabIndex = 13;
            this.metroTextBox3.Text = "x+1";
            this.metroTextBox3.UseSelectable = true;
            this.metroTextBox3.Visible = false;
            this.metroTextBox3.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox3.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroTextBox1
            // 
            // 
            // 
            // 
            this.metroTextBox1.CustomButton.Image = null;
            this.metroTextBox1.CustomButton.Location = new System.Drawing.Point(131, 1);
            this.metroTextBox1.CustomButton.Name = "";
            this.metroTextBox1.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.metroTextBox1.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.metroTextBox1.CustomButton.TabIndex = 1;
            this.metroTextBox1.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.metroTextBox1.CustomButton.UseSelectable = true;
            this.metroTextBox1.CustomButton.Visible = false;
            this.metroTextBox1.Lines = new string[] {
        "x^3+x^2+1"};
            this.metroTextBox1.Location = new System.Drawing.Point(3, 210);
            this.metroTextBox1.MaxLength = 32767;
            this.metroTextBox1.Name = "metroTextBox1";
            this.metroTextBox1.PasswordChar = '\0';
            this.metroTextBox1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.metroTextBox1.SelectedText = "";
            this.metroTextBox1.SelectionLength = 0;
            this.metroTextBox1.SelectionStart = 0;
            this.metroTextBox1.ShortcutsEnabled = true;
            this.metroTextBox1.Size = new System.Drawing.Size(153, 23);
            this.metroTextBox1.TabIndex = 12;
            this.metroTextBox1.Text = "x^3+x^2+1";
            this.metroTextBox1.UseSelectable = true;
            this.metroTextBox1.Visible = false;
            this.metroTextBox1.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.metroTextBox1.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(4, 188);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(79, 19);
            this.metroLabel9.TabIndex = 11;
            this.metroLabel9.Text = "Многочлен";
            this.metroLabel9.Visible = false;
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(323, 210);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(75, 23);
            this.metroButton2.TabIndex = 9;
            this.metroButton2.Text = "Проверить";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Visible = false;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(3, 34);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(103, 23);
            this.metroButton1.TabIndex = 8;
            this.metroButton1.Text = "Генерировать";
            this.metroToolTip1.SetToolTip(this.metroButton1, "Генерирует многочлены\r\nс заданной максимальной \r\nстепенью и сохраняет \r\nнеразложи" +
        "мые в файл.");
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // maxDegreeBox
            // 
            // 
            // 
            // 
            this.maxDegreeBox.CustomButton.Image = null;
            this.maxDegreeBox.CustomButton.Location = new System.Drawing.Point(81, 1);
            this.maxDegreeBox.CustomButton.Name = "";
            this.maxDegreeBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.maxDegreeBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.maxDegreeBox.CustomButton.TabIndex = 1;
            this.maxDegreeBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.maxDegreeBox.CustomButton.UseSelectable = true;
            this.maxDegreeBox.CustomButton.Visible = false;
            this.maxDegreeBox.Lines = new string[] {
        "2"};
            this.maxDegreeBox.Location = new System.Drawing.Point(182, 8);
            this.maxDegreeBox.MaxLength = 32767;
            this.maxDegreeBox.Name = "maxDegreeBox";
            this.maxDegreeBox.PasswordChar = '\0';
            this.maxDegreeBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.maxDegreeBox.SelectedText = "";
            this.maxDegreeBox.SelectionLength = 0;
            this.maxDegreeBox.SelectionStart = 0;
            this.maxDegreeBox.ShortcutsEnabled = true;
            this.maxDegreeBox.Size = new System.Drawing.Size(103, 23);
            this.maxDegreeBox.Style = MetroFramework.MetroColorStyle.Orange;
            this.maxDegreeBox.TabIndex = 3;
            this.maxDegreeBox.Text = "2";
            this.maxDegreeBox.UseSelectable = true;
            this.maxDegreeBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.maxDegreeBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(3, 12);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(153, 19);
            this.metroLabel1.TabIndex = 2;
            this.metroLabel1.Text = "Максимальная степень:";
            // 
            // metroTabPage3
            // 
            this.metroTabPage3.Controls.Add(this.pictureBox2);
            this.metroTabPage3.Controls.Add(this.metroLabel8);
            this.metroTabPage3.Controls.Add(this.modQBox);
            this.metroTabPage3.Controls.Add(this.metroLabel3);
            this.metroTabPage3.Controls.Add(this.polyQBox);
            this.metroTabPage3.Controls.Add(this.metroButton5);
            this.metroTabPage3.Controls.Add(this.metroLabel7);
            this.metroTabPage3.HorizontalScrollbarBarColor = true;
            this.metroTabPage3.HorizontalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.HorizontalScrollbarSize = 10;
            this.metroTabPage3.Location = new System.Drawing.Point(4, 38);
            this.metroTabPage3.Name = "metroTabPage3";
            this.metroTabPage3.Size = new System.Drawing.Size(414, 243);
            this.metroTabPage3.TabIndex = 2;
            this.metroTabPage3.Text = "Разложить";
            this.metroTabPage3.VerticalScrollbarBarColor = true;
            this.metroTabPage3.VerticalScrollbarHighlightOnWheel = false;
            this.metroTabPage3.VerticalScrollbarSize = 10;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(4, 157);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(407, 50);
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(3, 134);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(67, 19);
            this.metroLabel8.TabIndex = 13;
            this.metroLabel8.Text = "Результат:";
            // 
            // modQBox
            // 
            // 
            // 
            // 
            this.modQBox.CustomButton.AccessibleName = "asd";
            this.modQBox.CustomButton.Image = null;
            this.modQBox.CustomButton.Location = new System.Drawing.Point(110, 1);
            this.modQBox.CustomButton.Name = "";
            this.modQBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.modQBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.modQBox.CustomButton.TabIndex = 1;
            this.modQBox.CustomButton.Text = "Проверить";
            this.modQBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.modQBox.CustomButton.UseSelectable = true;
            this.modQBox.CustomButton.Visible = false;
            this.modQBox.IconRight = true;
            this.modQBox.Lines = new string[] {
        "0"};
            this.modQBox.Location = new System.Drawing.Point(3, 75);
            this.modQBox.MaxLength = 32767;
            this.modQBox.Name = "modQBox";
            this.modQBox.PasswordChar = '\0';
            this.modQBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.modQBox.SelectedText = "";
            this.modQBox.SelectionLength = 0;
            this.modQBox.SelectionStart = 0;
            this.modQBox.ShortcutsEnabled = true;
            this.modQBox.ShowClearButton = true;
            this.modQBox.Size = new System.Drawing.Size(132, 23);
            this.modQBox.Style = MetroFramework.MetroColorStyle.Orange;
            this.modQBox.TabIndex = 12;
            this.modQBox.Text = "0";
            this.metroToolTip1.SetToolTip(this.modQBox, "Введите 0, чтобы\r\nразложить над полем Q");
            this.modQBox.UseSelectable = true;
            this.modQBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.modQBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.Location = new System.Drawing.Point(3, 53);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(66, 19);
            this.metroLabel3.TabIndex = 11;
            this.metroLabel3.Text = "В кольце:";
            // 
            // polyQBox
            // 
            // 
            // 
            // 
            this.polyQBox.CustomButton.AccessibleName = "asd";
            this.polyQBox.CustomButton.Image = null;
            this.polyQBox.CustomButton.Location = new System.Drawing.Point(386, 1);
            this.polyQBox.CustomButton.Name = "";
            this.polyQBox.CustomButton.Size = new System.Drawing.Size(21, 21);
            this.polyQBox.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.polyQBox.CustomButton.TabIndex = 1;
            this.polyQBox.CustomButton.Text = "Проверить";
            this.polyQBox.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.polyQBox.CustomButton.UseSelectable = true;
            this.polyQBox.CustomButton.Visible = false;
            this.polyQBox.IconRight = true;
            this.polyQBox.Lines = new string[] {
        "2x+2"};
            this.polyQBox.Location = new System.Drawing.Point(3, 27);
            this.polyQBox.MaxLength = 32767;
            this.polyQBox.Name = "polyQBox";
            this.polyQBox.PasswordChar = '\0';
            this.polyQBox.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.polyQBox.SelectedText = "";
            this.polyQBox.SelectionLength = 0;
            this.polyQBox.SelectionStart = 0;
            this.polyQBox.ShortcutsEnabled = true;
            this.polyQBox.ShowClearButton = true;
            this.polyQBox.Size = new System.Drawing.Size(408, 23);
            this.polyQBox.Style = MetroFramework.MetroColorStyle.Orange;
            this.polyQBox.TabIndex = 10;
            this.polyQBox.Text = "2x+2";
            this.polyQBox.UseSelectable = true;
            this.polyQBox.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.polyQBox.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // metroButton5
            // 
            this.metroButton5.Location = new System.Drawing.Point(3, 104);
            this.metroButton5.Name = "metroButton5";
            this.metroButton5.Size = new System.Drawing.Size(75, 23);
            this.metroButton5.TabIndex = 9;
            this.metroButton5.Text = "Разложить";
            this.metroToolTip1.SetToolTip(this.metroButton5, "Если разложение не появилось,\r\nно кнопка разблокировалась,\r\nзначит мн-н неразложи" +
        "м");
            this.metroButton5.UseSelectable = true;
            this.metroButton5.Click += new System.EventHandler(this.metroButton5_Click);
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.Location = new System.Drawing.Point(3, 5);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(132, 19);
            this.metroLabel7.TabIndex = 8;
            this.metroLabel7.Text = "Введите многочлен:";
            // 
            // metroToggle1
            // 
            this.metroToggle1.AutoSize = true;
            this.metroToggle1.DisplayStatus = false;
            this.metroToggle1.Location = new System.Drawing.Point(172, 14);
            this.metroToggle1.Name = "metroToggle1";
            this.metroToggle1.Size = new System.Drawing.Size(50, 17);
            this.metroToggle1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroToggle1.TabIndex = 1;
            this.metroToggle1.Text = "Off";
            this.metroToggle1.UseSelectable = true;
            this.metroToggle1.CheckedChanged += new System.EventHandler(this.metroToggle1_CheckedChanged);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(12, 14);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(154, 19);
            this.metroLabel2.TabIndex = 2;
            this.metroLabel2.Text = "Локальные вычисления";
            this.metroToolTip1.SetToolTip(this.metroLabel2, "Включайте, только если на \r\nвашем компьютере установлена\r\nлокальная копия Wolfram" +
        " Mathematica.");
            // 
            // metroToolTip1
            // 
            this.metroToolTip1.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroToolTip1.StyleManager = null;
            this.metroToolTip1.Theme = MetroFramework.MetroThemeStyle.Light;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "polynomials.json";
            // 
            // saveFileDialog1
            // 
            this.saveFileDialog1.FileName = "polynomials.json";
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(12, 37);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(148, 19);
            this.metroLabel5.TabIndex = 4;
            this.metroLabel5.Text = "Использовать Wolfram";
            this.metroToolTip1.SetToolTip(this.metroLabel5, "Включайте, только если на \r\nвашем компьютере установлена\r\nлокальная копия Wolfram" +
        " Mathematica.");
            // 
            // metroToggle2
            // 
            this.metroToggle2.AutoSize = true;
            this.metroToggle2.DisplayStatus = false;
            this.metroToggle2.Location = new System.Drawing.Point(172, 37);
            this.metroToggle2.Name = "metroToggle2";
            this.metroToggle2.Size = new System.Drawing.Size(50, 17);
            this.metroToggle2.Style = MetroFramework.MetroColorStyle.Orange;
            this.metroToggle2.TabIndex = 3;
            this.metroToggle2.Text = "Off";
            this.metroToggle2.UseSelectable = true;
            this.metroToggle2.CheckedChanged += new System.EventHandler(this.metroToggle2_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 339);
            this.Controls.Add(this.metroLabel5);
            this.Controls.Add(this.metroToggle2);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroToggle1);
            this.Controls.Add(this.metroTabControl1);
            this.DisplayHeader = false;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Padding = new System.Windows.Forms.Padding(20, 30, 20, 20);
            this.Resizable = false;
            this.Style = MetroFramework.MetroColorStyle.Orange;
            this.Text = "Form1";
            this.metroTabControl1.ResumeLayout(false);
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.metroTabPage3.ResumeLayout(false);
            this.metroTabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public Wolfram.NETLink.MathKernel mathKernel1;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroTextBox maxDegreeBox;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroToggle metroToggle1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Components.MetroToolTip metroToolTip1;
        private MetroFramework.Controls.MetroButton metroButton3;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroTextBox polynomialBox;
        private MetroFramework.Controls.MetroButton metroButton4;
        private MetroFramework.Controls.MetroTextBox metroTextBox2;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private MetroFramework.Controls.MetroTabPage metroTabPage3;
        private MetroFramework.Controls.MetroTextBox modQBox;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox polyQBox;
        private MetroFramework.Controls.MetroButton metroButton5;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.PictureBox pictureBox2;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel metroLabel10;
        private MetroFramework.Controls.MetroTextBox metroTextBox3;
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private MetroFramework.Controls.MetroButton metroButton6;
        private MetroFramework.Controls.MetroTextBox metroTextBox4;
        private MetroFramework.Controls.MetroLabel metroLabel11;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroToggle metroToggle2;
    }
}

